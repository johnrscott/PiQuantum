# Compilers
CXX = g++

# Flags to the C++ compiler
CFLAGS = -g3 -std=c++14 -O2 -Wall -pthread

# Linker flags
LFLAGS = -lwiringPi

# Source search paths (locations containing .cpp/.hpp)
VPATH = src

# List output executables with object file dependencies
piquantum.bin: main.o state.o spi.o interface.o wpi.o io.o controller.o

# Make the documentation
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = sphinx
BUILDDIR      = docs/sphinx
.PHONY: docs
docs:
	doxygen
	@$(SPHINXBUILD) -M html "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# Dependency directories
DEPS = deps
OBJDIR = obj

# Include makefile patterns and dependency information
DFLAGS = -MMD -MT $*.o -MF $(DEPS)/$*.d

$(OBJDIR) $(DEPS):
	mkdir -p $@

vpath %.o $(OBJDIR)

define fixPath
$(addprefix $(OBJDIR)/, $(notdir $(filter %.o, $(1))))
endef

# Link
%:
	$(CXX) $(CFLAGS) $(call fixPath, $^) -o $@ $(LFLAGS)

# Compile
MAKEFILES = Makefile
%.o: %.cpp $(MAKEFILES) | $(OBJDIR) $(DEPS)
	$(CXX) $(CFLAGS) $(IFLAGS) $(DFLAGS) -c $< -o $(OBJDIR)/$@

.PHONY: clean
clean:
	-rm -rf $(OBJDIR)/*.o  $(DEPS)/*.d *.bin

-include $(wildcard $(DEPS)/*.d)

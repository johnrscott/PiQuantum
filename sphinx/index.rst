.. PiQuantum documentation main file. Put content for
   the pages in the sections/ directory.
   
Welcome to PiQuantum's documentation!
=====================================

.. warning::
   This documentation is not finished yet. Do not trust anything!

This website contains the full documentation for the PiQuantum project, a 16-qubit hardware quantum computer simulator that runs on a Raspberry Pi. An image of the PiQuantum board is shown below.
   
.. image:: images/board.png
  :width: 400
  :align: center
  :alt: Top view of the PiQuantum board

This documentation explains what PiQuantum is, how to build it, and how to use it. There is also information available about how it works (ref sections).

Code Repository
###############

The GitLab page for the project is `here <https://gitlab.com/johnrscott/piquantum>`_.
	
.. toctree::
   :maxdepth: 2
   :caption: Overview

   sections/overview

.. toctree::
   :maxdepth: 2
   :caption: Using the Simulator

   sections/guide
   
.. _rpi-setup:

.. toctree::
   :maxdepth: 2
   :caption: Setting up the RPi

   sections/setup
	     
.. toctree::
   :maxdepth: 2
   :caption: Building the Electronics

   sections/hardware

.. toctree::
   :maxdepth: 2
   :caption: Development

   sections/devel


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

`timescale 1ps/1ps

module memory (
  // Inouts
  inout [15:0]       ddr3_dq,
  inout [1:0]        ddr3_dqs_n,
  inout [1:0]        ddr3_dqs_p,
  // Outputs
  output [13:0]     ddr3_addr,
  output [2:0]        ddr3_ba,
  output            ddr3_ras_n,
  output            ddr3_cas_n,
  output            ddr3_we_n,
  output            ddr3_reset_n,
  output [0:0]       ddr3_ck_p,
  output [0:0]       ddr3_ck_n,
  output [0:0]       ddr3_cke,
  output [0:0]        ddr3_cs_n,
  output [1:0]     ddr3_dm,
  output [0:0]       ddr3_odt,
  
  // Inputs
  // Single-ended system clock
  input             sys_clk_i,
  // Single-ended iodelayctrl clk (reference clock)
  input             clk_ref_i,
  
  input			sys_rst,
  
  input start
);

    // Address for the request currently
    // being submitted. Memory is exposed as
    // a flat address space (28-bits corresponds
    // to 256MiB)
    reg[27:0] app_addr;
    
    // Command code. b00 = write, b01 = read
    reg[2:0] app_cmd;
    parameter CMD_WRITE = 0, CMD_READ = 1;
    
    // Signal that initiates a request. First,
    // load the values of app_addr, app_cmd
    // and app_hi_pri. Then assert the app_en
    // signal to start the handshake. Expect
    // app_rdy to be asserted in reply.
    reg app_en;
    
    // Provides the data currently being written
    // to the external memory
    reg[127:0]  app_wdf_data;
    
    // Assert this signal to indicate that the
    // data currently being written on app_wdf_data
    // is the last data for the current request
    reg app_wdf_end;
    
    // Set which bits of app_wdf_data should be
    // written to external memory and which should
    // remain in the current state
    reg[15:0] app_wdf_mask;
    
    // This indicates that the data on app_wdf_data
    // is valid.
    reg app_wdf_wren;
    
    // Contains data read from the external memory
    wire[127:0] app_rd_data;
    
    // This is set high when the data in app_rd_data
    // is the last data for the current request
    wire app_rd_data_end;
    
    // This input set high when the app_rd_data
    // is valid.
    wire app_rd_data_valid;
    
    // This indicates whether the command being
    // requested by the user is accepted. If the
    // UI does not assert this signal after the 
    // app_en is asserted (the next clock cycle?),
    // then the command must be retried (by 
    // clearing and re-asserting app_en?)
    wire app_rdy;
    
    // This indicates that the write-data FIFO
    // is ready to receive data
    wire app_wdf_rdy;
    
    // Setting this high requests a DRAM refresh
    wire app_ref_req;
    wire app_ref_ack;
        
    // This command requsts a ZQ calibration.
    wire app_zq_req;
    wire app_zq_ack;
    
    wire ui_clk;
    wire ui_clk_sync_rst;
    wire init_calib_complete;
    wire[11:0] device_temp;
    
    // Unused signals
    assign app_ref_req = 0;
    assign app_zq_req = 0;   
    
    // Memory interface
    mig_7series_0 mig_0 (
        
        // Inouts
        .ddr3_dq(ddr3_dq),
        .ddr3_dqs_n(ddr3_dqs_n),
        .ddr3_dqs_p(ddr3_dqs_p),
        // Outputs
        .ddr3_addr(ddr3_addr),
        .ddr3_ba(ddr3_ba),
        .ddr3_ras_n(ddr3_ras_n),
        .ddr3_cas_n(ddr3_cas_n),
        .ddr3_we_n(ddr3_we_n),
        .ddr3_reset_n(ddr3_reset_n),
        .ddr3_ck_p(ddr3_ck_p),
        .ddr3_ck_n(ddr3_ck_n),
        .ddr3_cke(ddr3_cke),
        .ddr3_cs_n(ddr3_cs_n),
        .ddr3_dm(ddr3_dm),
        .ddr3_odt(ddr3_odt),
        // Inputs
        // Single-ended system clock
        .sys_clk_i(sys_clk_i),
        // Single-ended iodelayctrl clk (reference clock)
        . clk_ref_i(clk_ref_i),
        // user interface signals
        .app_addr(app_addr),
        .app_cmd(app_cmd),
        .app_en(app_en),
        .app_wdf_data(app_wdf_data),
        .app_wdf_end(app_wdf_end),
        .app_wdf_mask(app_wdf_mask),
        .app_wdf_wren(app_wdf_wren),
        .app_rd_data(app_rd_data),
        .app_rd_data_end(app_rd_data_end),
        .app_rd_data_valid(app_rd_data_valid),
        .app_rdy(app_rdy),
        .app_wdf_rdy(app_wdf_rdy),
        .app_sr_req(0),
        .app_ref_req(app_ref_req),
        .app_zq_req(app_zq_req),
        .app_ref_ack(app_ref_ack),
        .app_zq_ack(app_zq_ack),
        .ui_clk(ui_clk),
        .ui_clk_sync_rst(ui_clk_sync_rst),
        .init_calib_complete(init_calib_complete),
        .device_temp(device_temp),
        .sys_rst(sys_rst) 
    );
    
    
    reg[2:0] state;
    parameter CALIB = 0, IDLE = 1, WRITE_EN = 2, WRITE_RDY = 3;
    
    initial state = CALIB;
    always @ (posedge ui_clk, negedge sys_rst)
    begin
        if (!sys_rst) begin
            state <= CALIB;
            app_addr <= 0;
            app_cmd <= CMD_READ;
            app_en <= 0;
            app_wdf_data <= 0;
            app_wdf_end <= 0;
            app_wdf_mask <= 0;
            app_wdf_wren <= 0;
        end
        else begin
            case (state)
                CALIB:
                    if (init_calib_complete)
                        state <= IDLE;
                IDLE:
                    if (start)
                        state <= WRITE_EN;
                WRITE_EN: begin
    
                    // Start command
                    app_addr <= 0;
                    app_cmd <= CMD_WRITE;
                    app_en <= 1; // Send command
    
                    // Start data transfer
                    app_wdf_data <= 3;
                    app_wdf_end <= 1; // Mark data as last
                    app_wdf_wren <= 1; // Mark data as valid
    
                    // Update state
                    state <= WRITE_RDY;
    
                end
    
                WRITE_RDY: begin
                    // Check if command is accepted
                    if (app_en && app_rdy) begin
                        // Reset command
                        app_en <= 0;
                    end
                    if (app_wdf_wren && app_wdf_rdy) begin
                        // Reset data
                        app_wdf_wren <= 0;
                    end
                    if (!app_wdf_wren && !app_en) begin
                        // Update state to idle
                        state <= IDLE;
                    end       
                end
            endcase;
        end          
    end


endmodule



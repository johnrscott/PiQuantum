# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct /home/jrs/Documents/git/piquantum/hardware/arty-a7/sim_vitis/sim_platform/platform.tcl
# 
# OR launch xsct and run below command.
# source /home/jrs/Documents/git/piquantum/hardware/arty-a7/sim_vitis/sim_platform/platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {sim_platform}\
-hw {/home/jrs/Documents/git/piquantum/hardware/arty-a7/sim/sim1_wrapper.xsa}\
-proc {microblaze_0} -os {standalone} -fsbl-target {psu_cortexa53_0} -out {/home/jrs/Documents/git/piquantum/hardware/arty-a7/sim_vitis}

platform write
platform generate -domains 
platform active {sim_platform}
platform generate
platform clean
platform generate
platform clean
platform generate
platform config -updatehw {/home/jrs/Documents/git/piquantum/hardware/arty-a7/sim/sim1_wrapper.xsa}
platform generate -domains 
bsp reload
catch {bsp regenerate}
platform generate -domains standalone_domain 
platform config -updatehw {/home/jrs/Documents/git/piquantum/hardware/arty-a7/sim/sim1_wrapper.xsa}
platform generate -domains 
bsp reload
catch {bsp regenerate}
platform generate -domains standalone_domain 
platform active {sim_platform}
platform generate -domains 
platform config -updatehw {/home/jrs/Documents/git/piquantum/hardware/arty-a7/sim/sim1_wrapper.xsa}
platform generate -domains 
platform active {sim_platform}
platform generate -domains 
platform config -updatehw {/home/jrs/Documents/git/piquantum/hardware/arty-a7/sim/sim1_wrapper.xsa}
platform generate -domains 
platform config -updatehw {/home/jrs/Documents/git/piquantum/hardware/arty-a7/sim/sim1_wrapper.xsa}
platform generate -domains 
platform write

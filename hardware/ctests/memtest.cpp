#include <iostream>
#include <cstdint>
#include <vector>

#define MEM_SIZE 0x10000000

int main()
{   
    std::vector<uint8_t> mem(MEM_SIZE);

    std::uint32_t * mem_base = (std::uint32_t *)&mem[0];
    
    // Write test
    for (int n = 0; n < MEM_SIZE/4; n++) {
    	// Memory val
	std::uint32_t val = n & 0xffff;
    	// Write memory value
    	* (mem_base + n) = val;
    }
    std::cout << "Finished write" << std::endl;

    // Write test
    for (int n = 0; n < MEM_SIZE/4; n++) {
    	// Memory val
	std::uint32_t val = n & 0xffff;
    	// Check memory value
    	if (val != * (mem_base + n)) {
	    std::cout << "Fail" << std::endl;
    	}
    }
    std::cout << "Finished read" << std::endl;
    

}

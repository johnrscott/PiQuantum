/* 
 * Copyright (C) 2021 Oliver Thomas and John Scott.
 *
 * This file is part of PiQuantum, the Rasbperry Pi quantum computer
 * simulator.
 *
 * PiQuantum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PiQuantum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PiQuantum.  If not, see <https://www.gnu.org/licenses/>.
 */

/** 
 * \file wpi.cpp
 * \authors J Scott, O Thomas
 * \date Feb 2019 
 *
 * \detail Source file for WiringPi functions
 *
 */

#include "wpi.hpp"

// Initialise WiringPi static variable to zero
int WiringPi::setup = 0;

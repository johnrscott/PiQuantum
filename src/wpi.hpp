/* 
 * Copyright (C) 2021 Oliver Thomas and John Scott.
 *
 * This file is part of PiQuantum, the Rasbperry Pi quantum computer
 * simulator.
 *
 * PiQuantum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PiQuantum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PiQuantum.  If not, see <https://www.gnu.org/licenses/>.
 */

/** 
 * \file wpi.hpp
 * \authors J Scott, O Thomas
 * \date Feb 2019 
 *
 * \detail Header file for WiringPi functions
 *
 */

#ifndef WPI_HPP
#define WPI_HPP

#include <wiringPi.h>

/** 
 * \brief Class for initialising wiringPi
 *
 * \detail Everything that uses input/output
 * using wiringPi should include a WiringPi
 * private data member. That will make sure 
 * that the necessary setup routines get called
 * before anything starts using input/output
 * functions
 *
 */
class WiringPi 
{
    private:
        static int setup; // Becomes 1 when wPi is initialised 

    public:
        WiringPi()
        {
            if(setup == 0)
            {
                // Need to call this setup function for wiringPi before
                // using any of its functions. Use wiringPi pin conventions
                // (see the reference -> setup section of the wiringPi website).
                wiringPiSetup();      
            }
        }
};

#endif

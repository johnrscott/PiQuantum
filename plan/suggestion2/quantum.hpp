/**
 * \file quantum.hpp
 * \brief Contains the quantum system
 *
 */

/**
 * \brief The main quantum simulator class
 *
 * The job if this class is to hold a copy of a state vector, manipulate
 * the state vector, and return properties of the state vector.
 *
 * This class is designed in such a way that none of its internal workings
 * depend on any of the other parts of the program. 
 *
 * The only part of the class which is directly related to the outside is
 * Colour. This might not need to be an actual colour, but I think it makes
 * sense to have this class handle the qubit state-to-"colour" mapping in
 * general (even if the output class decides to swap blue/red, for example).
 */
class QuantumSystem
{

public:
    /**
     * \brief Make a QuantumSystem object
     * Should initialise in the all zero state.
     * \param num_qubits The number of qubits to simulate
     */
    QuantumSystem(unsigned num_qubits);

    // General control
    void reset(); ///< Reset to the all zero state
    
    // Gates
    void hadamard(unsigned targ); ///< Hadamard gate
    void pauliX(unsigned targ);  ///< Pauli X gate (bit flip)
    void pauliY(unsigned targ);  ///< Pauli Y gate
    void pauliZ(unsigned targ);  ///< Pauli Z gate (phase flip)
    void controlledNot(unsigned ctrl, unsigned targ); ///< Controlled-not gate

    // Measurement
    void measureQubit(unsigned targ); ///< Measure a particular qubit
    
    /**
     * \brief Get the colour of a particular qubit
     * Returns the colour that can be used to display a colour for
     * a qubit on the output device.
     */
    Colour getQubitColour(unsigned targ) const;

    /**
     * \brief Get the cycle pattern for all the qubits
     * Returns a vector of measurement outcomes suitable for displaying
     * cycle mode on the output device.
     */
    std::vector<Colour> getCyclePattern() const;
};

/**
 * \brief Wrapper for the QuantumSystem class
 *
 * The point of this class is to hold an instance of the QuantumSystem
 * class and provide control access to the class from other wrapper classes 
 * in the system.
 * 
 */
class QuantumSystemWrapper
{
    QuantumSystem qsys; ///< Instance of the quantum system
    std::shared_ptr<OutputDeviceWrapper> outdev;
    
    /// Queue for commands to the QuantumSystem
    std::queue<QuantumCmd> commands;

    /**
     * \brief A thread to read the queues and operate on the QuantumSystem
     */
    std::thread worker;

    /**
     * \brief Service the command queue in the worker thread
     *
     * The purpose of this function is to run an infinite while loop
     * which checks the commands queue and applies operations to the
     * QuantumSystem accordingly. It might also send commands to the
     * output device using the cmdOutputDevice function
     */
    void serviceCommands();

    /**
     * \brief Send a command to the output device
     *
     * This doesn't need to be public because it shouldn't be possible
     * for a user of this class to directly send commands to the output
     * device.
     *
     * Since the only user of this function is this class, it also 
     * probably doesn't need to be thread safe (to be double checked!)
     */
    void cmdOutputDevice();
    
public:

    /**
     * \brief Construct the wrapper
     *
     */
    QuantumSystemWrapper(std::shared_ptr<OutputDeviceWrapper> outdev);

    /**
     * \brief Send a command to the quantum device in this wrapper
     *
     * Since this function might be called simultaneously from several
     * different classes (for example, two different Input classes), it
     * is important that it is thread safe. (Locks a mutex or something
     * similar for accesses to the command queue.)
     */
    void pushCommand();
};
    

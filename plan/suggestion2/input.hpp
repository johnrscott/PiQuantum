/**
 * \file input.hpp
 * \brief Contains the class for all input devices
 *
 */

/**
 * \brief Abstract class for handling user input
 *
 * This class is the model for all input devices. It takes the input and
 * directs it to either the QuantumSystem or the OutputDevice via their
 * wrapper classes.
 *
 * Via the public cmd functions, it is possible for internal input sources
 * and external source to generate input events. By implementing them as
 * public functions of this abstract base class, it will not be necessary
 * for derived classes to worry about the actual quantum system and
 * output device wrappers.
 *
 * An important feature of this class is that it does not receive information
 * from other classes, it only sends information. I think this should be OK
 * since an input device by its nature only gives information to a system.
 */
class Input
{
    std::shared_ptr<QuantumSystemWrapper> qsys;
    std::shared_ptr<OutputDeviceWrapper> outdev;
    
    /**
     * \brief Worker thread that listens for user input
     */
    std::thread worker;

    /**
     * \brief Check for user input
     *
     * This function should be set running using the worker thread. 
     * It is a virtual function because the implementation will depend
     * on what the input device actually is.
     *
     * The implementation of this function in derived classes should
     * be an infinite while loop which listens for user inputs and 
     * then calls the cmd* functions below to send a command to the
     * QuantumSystem or the OutputDevice.
     */
    virtual void readInput() = 0;
    
public:

    /**
     * \brief Construct an input deivce 
     *
     * By putting the thread in this abstract base class, and making it
     * execute the readInput function, it should be possible for derived
     * classes not to worry about the threading stuff. All they need to
     * do is implement the readInput function.
     */
    Input(std::shared_ptr<QuantumSystemWrapper> qsys,
	  std::shared_ptr<OutputDeviceWrapper> outdev);

    /**
     * \brief Send a command to the QuantumSystem
     */
    void cmdQuantumSystem(const QuantumCmd & cmd);

    /**
     * \brief Send a command to the OutputDevice
     */
    void cmdOutputDevice(const OutputCmd & cmd);
};

/**
 * \brief Keyboard device input
 *
 * The main feature of this class is the implementation of 
 */
class KeyboardInput : public Input
{

    /**
     * \brief Read keyboard input
     *
     * Hopefully (to be checked thoroughly!), this concrete implementation
     * of the virtual function in the base class will be the one that
     * ends up running in the thread, if everything is set up right.
     *
     * Since the cmd* functions of the base class are public, they can
     * be called here in this function to implement the desired behaviour
     * of the input device.
     */
    void readInput();
    
public:

    /**
     * \brief Construct a keyboard input object
     */
    KeyboardInput(std::shared_ptr<QuantumSystemWrapper> qsys,
		  std::shared_ptr<OutputDeviceWrapper> output);
};

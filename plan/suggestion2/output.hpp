/**
 * \file output.hpp
 * \brief Contains the OutputDevice class
 *
 */


/**
 * \brief Abstract base class for output devices
 *
 * This class contains the interface for any output device (e.g. terminal 
 * output, graphics, or hardware board).
 *
 * Similarly to the QuantumSystem, this class does not directly interface
 * with other classes in the program. Instead, it is contained within a
 * wrapper (see OutputDeviceWrapper) which receives commands from other
 * parts of the program and controls the output device.
 *
 * The purpose of this class is to provide the functions required for
 * displaying output.
 * 
 */
class OutputDevice
{
public:

    /**
     * \brief Construct an OutputDevice
     * \todo What parameters does this take?
     */
    OutputDevice();

    /**
     * \brief Set the colour of a specific qubit
     */
    virtual void setColour(unsigned qubit, const Colour & col) = 0;

    /**
     * \brief Select a qubit (e.g. make it flash, or something else)
     */
    virtual void selectQubit(unsigned qubit) = 0;
};
